Source: banshee-community-extensions
Section: sound
Priority: optional
Maintainer: Debian CLI Applications Team <pkg-cli-apps-team@lists.alioth.debian.org>
Uploaders: Chow Loong Jin <hyperair@debian.org>
Build-Depends: debhelper (>= 7.0.50),
               dh-autoreconf,
               libgconf2-dev,
               cli-common-dev (>= 0.5.7),
               mono-devel (>= 2.4.2.3),
               autotools-dev,
               lsb-release,
               banshee (>= 2.4.0),
               pkg-config,
               liblircclient-dev (>= 0.8.6),
               libglib2.0-dev,
               libfftw3-dev,
               libsqlite3-dev (>= 3.3),
               libgstreamer0.10-dev (>= 0.10.15),
               libgstreamer-plugins-base0.10-dev (>= 0.10.15),
               libsamplerate0-dev,
               intltool (>= 0.35),
               libgconf2.0-cil-dev,
               libglib2.0-cil-dev,
               libgtk2.0-cil-dev,
               libtaglib-cil-dev,
               libdbus-glib1.0-cil-dev,
               libdbus1.0-cil-dev,
               libmono-addins-cil-dev,
               libnotify-cil-dev,
               libtaoframework-opengl-cil-dev (>= 2.1),
               libtaoframework-freeglut-cil-dev,
               libtaoframework-sdl-cil-dev (>= 1.2.13),
               mono-jay,
               gnome-doc-utils (>= 0.17.3),
               libzeitgeist-cil-dev
Standards-Version: 3.9.3
Vcs-Git: git://git.debian.org/git/pkg-cli-apps/packages/banshee-community-extensions.git
Vcs-Browser: http://git.debian.org/?p=pkg-cli-apps/packages/banshee-community-extensions.git
Homepage: http://gitorious.org/banshee-community-extensions

Package: banshee-community-extensions
Architecture: all
Enhances: banshee
Recommends: banshee-extension-alarm (>= ${source:Version}),
            banshee-extension-albumartwriter (>= ${source:Version}),
            banshee-extension-ampache (>= ${source:Version}),
            banshee-extension-awn (>= ${source:Version}),
            banshee-extension-coverwallpaper (>= ${source:Version}),
            banshee-extension-duplicatesongdetector (>= ${source:Version}),
            banshee-extension-foldersync (>= ${source:Version}),
            banshee-extension-jamendo (>= ${source:Version}),
            banshee-extension-lastfmfingerprint (>= ${source:Version}),
            banshee-extension-lcd (>= ${source:Version}),
            banshee-extension-lirc (>= ${source:Version}),
            banshee-extension-liveradio (>= ${source:Version}),
            banshee-extension-lyrics (>= ${source:Version}),
            banshee-extension-magnatune (>= ${source:Version}),
            banshee-extension-mirage (>= ${source:Version}),
            banshee-extension-openvp (>= ${source:Version}),
            banshee-extension-radiostationfetcher (>= ${source:Version}),
            banshee-extension-randombylastfm (>= ${source:Version}),
            banshee-extension-streamrecorder (>= ${source:Version}),
            banshee-extension-telepathy (>= ${source:Version}),
            banshee-extension-zeitgeistdataprovider (>= ${source:Version})
Depends: banshee-extensions-common (= ${source:Version}),
         ${misc:Depends}
Description: set of community contributed extensions for Banshee
 This package is a metapackage which depends on the current versions of all the
 extensions included in the Banshee Community Extensions pack.

Package: banshee-extensions-common
Architecture: all
Depends: banshee (>= ${banshee:Version}),
         ${misc:Depends}
Description: common files for banshee-community-extensions
 This package contains the common files for all the extensions in the Banshee
 Community Extension. You should not install this package directly, but instead
 install one of other extension packages.

Package: banshee-extension-alarm
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Alarm extension for Banshee
 Banshee Alarm Extension is an extension for Banshee which allows alarms to be
 set in Banshee. Supported features include:
  * Wake up time
  * Ascending volume alarm with configurable start and end volumes as well as
    duration
  * Sleep timer
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-albumartwriter
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Album art writer extension for Banshee
 This package contains the Album Art Writer extension for Banshee which writes
 album art from the cache to the folder containing the music files.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-ampache
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Ampache extension for Banshee
 This package provides integration for Banshee with the Ampache web-based audio
 file management system, allowing Banshee to act as a frontend for Ampache
 servers.
 .
 Banshee is a media management and playback application for the GNOME desktop

Package: banshee-extension-awn
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Suggests: avant-window-navigator
Description: AWN integration extension for Banshee
 This package provides integration for Banshee with the Avant Window Navigator,
 which is a Mac OS X like dock for the GNOME desktop.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-coverwallpaper
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Cover wallpaper extension for Banshee
 CoverWallpaper is an extension for Banshee which automatically sets the
 desktop wallpaper to the cover art of the currently playing media.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-duplicatesongdetector
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Duplicate song detector extension for Banshee
 This package adds duplicate song detection to Banshee, allowing it to detect
 and remove duplicate songs in your library.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-foldersync
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Folder synchronization extension for Banshee
 FolderSync is an extension which copies and synchronizes music files from
 playlists into user-specified folders.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-jamendo
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Jamendo extension for Banshee
 This package provides integration with the Jamendo music service. Jamendo is a
 community of music authors and performers which makes free music available.
 This extension allows you to browse the Jamendo catalog, stream and download
 free music.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-karaoke
Enhances: banshee
Architecture: all
Depends: banshee-extension-streamrecorder (= ${source:Version}),
         ${cli:Depends}, ${shlibs:Depends}, ${misc:Depends}
Description: Karaoke extension for Banshee
 This package adds karaoke functionality to Banshee by allowing filtering the
 singers' voice out of the song as it is played.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-lastfmfingerprint
Enhances: banshee
Architecture: any
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${shlibs:Depends}, ${misc:Depends}
Description: Last.FM fingerprinting extension for Banshee
 This package provides additional integration between Last.FM and Banshee,
 allowing Banshee to automatically generate and audio fingerprint and correct
 your metadata by querying Last.FM for the correct metadata.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-lcd
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         lcdproc,
         ${cli:Depends}, ${misc:Depends}
Description: LCD display integration extension for Banshee
 This package provides integration with LCD displays for Banshee using the
 lcdproc display driver daemon.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-lirc
Architecture: any
Depends: banshee-extensions-common (= ${source:Version}),
         lirc,
         ${shlibs:Depends}, ${cli:Depends}, ${misc:Depends}
Description: LIRC integration extension for Banshee
 This package provides LIRC integration for Banshee, which will allow Banshee to
 be controlled using an infra-red device.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-liveradio
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: LiveRadio extension for Banshee
 The LiveRadio extension for Banshee is a browser for online Internet station
 directories integrated into the Banshee media player. It currently supports
 live365.com, shoutcast.com and xiph.org, but its plugin-like code structure can
 easily be extended to support just about any directory.
 .
 LiveRadio has the follwing features:
  * Browse and query three online internet radio directories:
     – live365.com
     – shoutcast.com
     – xiph.org
  * Retrieve stream information and play the stream
  * Add your favorite streams to your list of Internet Radio stations in Banshee
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-lyrics
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Lyrics extension for Banshee
 Banshee Lyrics Plugin is an extension which shows lyrics of songs played in
 Banshee. It supports downloading lyrics from:
  * http://lyrc.com.ar
  * http://lyriky.com
  * http://lyricwiki.org
  * http://www.autolyrics.com
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-magnatune
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Magnatune for Banshee
 This package provides support for listening to Magnatune streams using Banshee.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-mirage
Enhances: banshee
Architecture: any
Depends: banshee-extensions-common (= ${source:Version}),
         ${shlibs:Depends}, ${cli:Depends}, ${misc:Depends}
Description: Automatic Playlist Generation extension for Banshee
 Mirage is a ready-to-try implementation of the latest research
 in automatic playlist generation and music similarity. Mirage
 analyzes your music collection and computes similarity models
 for each song. After your music has been analyzed, Mirage is
 able to automatically generate playlists of similar music.
 .
 Mirage is far from being perfect! But it is a nice and easy way
 to rediscover your music collection. Imagine it's an automated
 radio station DJ playing songs from your collection and all songs
 somehow fit together.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-openvp
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${shlibs:Depends}, ${cli:Depends}, ${misc:Depends}
Description: visualizations extension for Banshee
 This package contains an extension for Banshee which adds various
 visualizations to the Now Playing source in Banshee.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-radiostationfetcher
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: radio station fetcher extension for Banshee
 This package contains an extension for Banshee which adds radio station
 fetching functionality for Banshee. Radio stations are fetched from:
  * http://www.shoutcast.com
  * http://www.xiph.org
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-randombylastfm
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Random By Last.FM extension for Banshee
 This package provide additional integration between Last.FM and Banshee,
 providing a new random mode which queries Last.FM for a list of similar artists
 to the song you are currently playing, then compares that list with your
 library to pick only artists that match.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-streamrecorder
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: StreamRecorder extension for Banshee
 This package provides the StreamRecorder extension for Banshee which will allow
 it to rip/record radio streams. It has the following features:
  * Recording "what you hear is what you get"
  * Multiple encoders configurable
  * Recorded stream injected with metadata tags
  * Automatic file splitting and naming for streams sending metadata tags for
    title and artist
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-telepathy
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         empathy (>= 2.27.91),
         telepathy-gabble (>= 0.9),
         telepathy-mission-control-5 (>= 5.3.1),
         ${cli:Depends}, ${misc:Depends}
Description: Telepathy extension for Banshee
 This extension provides integration between the Empathy instant messenger and
 Banshee. It provides the following features:
 .
  * Download your friends' Banshee library metadata and check out what they
    listen to, their ratings, BPM values, etc.
  * View your friends' playlists and export them to disk
  * Share what you're listening to with all your instant messaging friends by
    advertising the track, artist, and album of the currently playing track in
    Empathy's status message. This can be disabled.
  * Download your friends' music; one track at a time or a selection. You can
    cancel ones in progress, queued, individually or all at once. The sender has
    the option to cancel all in progress or queued transfers only. Both sender
    and receiver get a progress bar. File sharing can be disabled.
  * Stream your friends' music. This feature can be disabled.
 .
 Banshee is a media management and playback application for the GNOME desktop.

Package: banshee-extension-zeitgeistdataprovider
Enhances: banshee
Architecture: all
Depends: banshee-extensions-common (= ${source:Version}),
         ${cli:Depends}, ${misc:Depends}
Description: Zeitgeist data provider extension for Banshee
 This extension provides integration between Zeitgeist and Banshee, allowing
 your activity in Banshee to be tracked by Zeitgeist and hence displayed in the
 Activity Journal.
 .
 Banshee is a media management and playback application for the GNOME desktop.
